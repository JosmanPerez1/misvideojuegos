package com.example.josman.misvideojuegos;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

/**
 * Created by Josman on 17/11/2016.
 */

public class VideoJuegosBD extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "videojuegos.db";
    public static final String TABLE_NAME = "videojuegos";
    public static final String COL_TITULO = "titulo";
    public static final String COL_CATEGORIA = "categoria";
    public static final String COL_PRECIO = "precio";
    public static final String COL_DESARROLLADOR = "desarrollador";
    public static final String COL_DESCRIPCION = "descripcion";
    public static final String  COL_ID = "_id";

    // constructor
    public VideoJuegosBD(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createQuery =
                "CREATE TABLE " + TABLE_NAME + "("
                        + COL_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + COL_TITULO + " TEXT NOT NULL, "
                        + COL_CATEGORIA + " TEXT NOT NULL, "
                        + COL_PRECIO + " TEXT NOT NULL, "
                        + COL_DESARROLLADOR + " TEXT, "
                        + COL_DESCRIPCION + " TEXT);";
        db.execSQL(createQuery);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String upgradeQuery = "DROP TABLE IF EXIST "+ TABLE_NAME;
        db.execSQL(upgradeQuery);
    }

    public static long insertaVideoJuego(Context context, String titulo, String categoria, String precio, String desarrollador, String descripcion){
        SQLiteOpenHelper dbOpenHelper = new VideoJuegosBD(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        ContentValues valoresVideoJuegos = new ContentValues();
        valoresVideoJuegos.put(COL_TITULO, titulo);
        valoresVideoJuegos.put(COL_CATEGORIA, categoria);
        valoresVideoJuegos.put(COL_PRECIO, precio);
        valoresVideoJuegos.put(COL_DESARROLLADOR, desarrollador);
        valoresVideoJuegos.put(COL_DESCRIPCION, descripcion);

        long result = -1L ;
        try {
            result = database.insert(TABLE_NAME, null, valoresVideoJuegos);
            if (result != -1L){
                LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                Intent intentFilter = new Intent(VideoJuegoSLoader.ACTION_RELOAD_TABLE);
                broadcastManager.sendBroadcast(intentFilter);
            }

        } finally {
            dbOpenHelper.close();
        }
        return result;
    }

    public static Cursor devuelveTodos (Context context){
        SQLiteOpenHelper dbOpenHelper = new VideoJuegosBD(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();

        return  database.query(
                TABLE_NAME,
                new String[]{COL_TITULO, COL_CATEGORIA, COL_PRECIO, COL_DESARROLLADOR, COL_DESCRIPCION,BaseColumns._ID},
                null, null, null, null,
                COL_TITULO+" ASC");
    }
    public static Cursor devuelveConId(Context context, long identificador){
        SQLiteOpenHelper dbOpenHelper = new VideoJuegosBD(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();

            return  database.query(
                TABLE_NAME,
                new String[]{COL_TITULO, COL_CATEGORIA, COL_PRECIO, COL_DESARROLLADOR, COL_DESCRIPCION,BaseColumns._ID},
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(identificador)},
                null, null,
                COL_TITULO+" ASC");
    }

    public static int eliminaConId(Context context, long id){
        SQLiteOpenHelper dbOpenHelper = new VideoJuegosBD(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        int resultado = database.delete(
                TABLE_NAME,
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(id)});

        if (resultado != 0){

            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(VideoJuegoSLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }
        dbOpenHelper.close();
        return resultado;
    }

    public static long actualizaVideoJuego(Context context, String titulo, String categoria, String precio, String desarrollador, String descripcion, long id){

        SQLiteOpenHelper dbOpenHelper = new VideoJuegosBD(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valoresVideoJuegos = new ContentValues();
        valoresVideoJuegos.put(COL_TITULO, titulo);
        valoresVideoJuegos.put(COL_CATEGORIA, categoria);
        valoresVideoJuegos.put(COL_PRECIO, precio);
        valoresVideoJuegos.put(COL_DESARROLLADOR, desarrollador);
        valoresVideoJuegos.put(COL_DESCRIPCION, descripcion);


        long result = -1L;

        result = database.update(
                TABLE_NAME,
                valoresVideoJuegos,
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(id)}
        );
        if (result != 0){

            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(VideoJuegoSLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }

        dbOpenHelper.close();
        return result;
    }


}
