package com.example.josman.misvideojuegos;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.LocalBroadcastManager;
/**
 * Created by Josman on 17/11/2016.
 */

public class VideoJuegoSLoader extends AsyncTaskLoader<Cursor> {
    private Cursor mData;
    private VideoJuegosObserver mObserver;
    static final String ACTION_RELOAD_TABLE = "reloadData";

    public VideoJuegoSLoader(@NonNull Context context) {
        super(context);
    }

    @Override
    public Cursor loadInBackground() {
        Context context = getContext();
        Cursor cursor = VideoJuegosBD.devuelveTodos(context);
        return cursor;
    }

    @Override
    public void deliverResult(Cursor data) {
        if (isReset()) {
            // The Loader has been reset; ignore the result and invalidate the data.
            releaseResources(data);
            return;
        }

        // Hold a reference to the old data so it doesn't get garbage collected.
        // We must protect it until the new data has been delivered.
        Cursor oldData = mData;
        mData = data;

        if (isStarted()) {
            // If the Loader is in a started state, deliver the results to the
            // client. The superclass method does this for us.
            super.deliverResult(data);
        }

        // Invalidate the old data as we don't need it any more.
        if (oldData != null && oldData != data) {
            releaseResources(oldData);
        }
    }

    @Override
    public void onStartLoading(){
        if (mData != null) {
            // Deliver any previously loaded data immediately.
            deliverResult(mData);
        }

        // Begin monitoring the underlying data source.
        if (mObserver == null) {
            mObserver = new VideoJuegosObserver(this);
            mObserver.register();
        }

        if (takeContentChanged() || mData == null) {
            // When the observer detects a change, it should call onContentChanged()
            // on the Loader, which will cause the next call to takeContentChanged()
            // to return true. If this is ever the case (or if the current data is
            // null), we force a new load.
            forceLoad();
        }

    }

    @Override
    public void onStopLoading(){
        cancelLoad();
    }

    @Override
    protected void onReset() {
        onStopLoading();
        // At this point we can release the resources associated with 'mData'.
        if (mData != null) {
            releaseResources(mData);
            mData = null;
        }

        // The Loader is being reset, so we should stop monitoring for changes.
        if (mObserver != null) {
            mObserver.unregister();
            mObserver = null;
        }
    }

    @Override
    public void onCanceled(Cursor data) {
        // Attempt to cancel the current asynchronous load.
        super.onCanceled(data);

        // The load has been canceled, so we should release the resources
        // associated with 'data'.
        releaseResources(data);
    }

    private void releaseResources(Cursor data) {
        // For a simple List, there is nothing to do. For something like a Cursor, we
        // would close it in this method. All resources associated with the Loader
        // should be released here.
        if (data != null){
            data.close();
        }
    }

    public static class VideoJuegosObserver extends BroadcastReceiver{
        private final VideoJuegoSLoader loader;
        public VideoJuegosObserver(VideoJuegoSLoader loader){
            this.loader = loader;
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            loader.onContentChanged();
        }
        public void unregister(){
            LocalBroadcastManager.getInstance(loader.getContext()).unregisterReceiver(this);
        }
        public void register(){

            IntentFilter filter = new IntentFilter(ACTION_RELOAD_TABLE);
            LocalBroadcastManager.getInstance(loader.getContext()).registerReceiver(this, filter);

        }
    }

}
