package com.example.josman.misvideojuegos;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by Josman on 17/11/2016.
 */

public class MiAplicacion extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
