package com.example.josman.misvideojuegos;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

/**
 * Created by Josman on 16/11/2016.
 */

public class AgregarVideoJuego extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {
    private Long idET = -1L;
    private EditText tituloET;
    private EditText categoriaET;
    private EditText precioET;
    private EditText desarrolladorET;
    private EditText descripcionET;

        @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agregar_videojuego);

        tituloET = (EditText) findViewById(R.id.titulo_edit_text);
        categoriaET = (EditText) findViewById(R.id.categoria_edit_text);
        precioET = (EditText) findViewById(R.id.precio_edit_text);
        desarrolladorET = (EditText) findViewById(R.id.desarrollador_edit_text);
        descripcionET = (EditText) findViewById(R.id.descripcion_edit_text);
        idET = getIntent().getLongExtra(DetalleVideoJuego.EXTRA_VIDEOJUEGO_ID, -1L);
            if (idET != -1L){
                getSupportLoaderManager().initLoader(0, null, this);
            }
            findViewById(R.id.boton_insertar_videojuego).setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        String tituloVJ = tituloET.getText().toString();
        String categoriaVJ = categoriaET.getText().toString();
        String precioVJ = precioET.getText().toString();
        String desaVJ = desarrolladorET.getText().toString();
        String descVJ = descripcionET.getText().toString();


        if (tituloVJ.isEmpty() || categoriaVJ.isEmpty()|| precioVJ.isEmpty()){
            Toast.makeText(this, "Captura los datos obligatorios", Toast.LENGTH_SHORT).show();
        }else{
            new CreateVideoJuegoTask(this, tituloVJ, categoriaVJ, precioVJ, desaVJ, descVJ, idET).execute();
        }



    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new VideoJuegoLoader(this, idET);
        //return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data != null && data.moveToFirst()) {
            int tituloIndex = data.getColumnIndexOrThrow(VideoJuegosBD.COL_TITULO);
            String tituloVideoJuego = "Titulo: " + data.getString(tituloIndex);

            int categoriaIndex = data.getColumnIndexOrThrow(VideoJuegosBD.COL_CATEGORIA);
            String categoriaVJ = data.getString(categoriaIndex);

            int precioIndex = data.getColumnIndexOrThrow(VideoJuegosBD.COL_PRECIO);
            String precioVideoJuego = data.getString(precioIndex);

            int desarrolladorIndex = data.getColumnIndexOrThrow(VideoJuegosBD.COL_DESARROLLADOR);
            String desarrolladorVideoJuego = data.getString(desarrolladorIndex);

            int descripcionIndex = data.getColumnIndexOrThrow(VideoJuegosBD.COL_DESCRIPCION);
            String descripcionVideoJuego = data.getString(descripcionIndex);

            tituloET.setText(tituloVideoJuego);
            categoriaET.setText(categoriaVJ);
            precioET.setText(precioVideoJuego);
            desarrolladorET.setText(desarrolladorVideoJuego);
            descripcionET.setText(descripcionVideoJuego);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public static class CreateVideoJuegoTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private String vJTitulo, vJCategoria, vJPrecio, vJDesarrollador, vJDescripcion;
        private long vJId;

        public CreateVideoJuegoTask(Activity activity, String titulo, String categoria, String precio, String desarrollador, String descripcion, long id){
            weakActivity = new WeakReference<Activity>(activity);
            vJTitulo = titulo;
            vJCategoria = categoria;
            vJPrecio = precio;
            vJDesarrollador = desarrollador;
            vJDescripcion = descripcion;
            vJId = id;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            Boolean exito = false;
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();
            if (vJId != -1L){
                long filasAfectadas = VideoJuegosBD.actualizaVideoJuego(appContext,vJTitulo,vJCategoria,vJPrecio, vJDesarrollador,vJDescripcion, vJId);

                exito = (filasAfectadas !=0);
            }else{

                long id = VideoJuegosBD.insertaVideoJuego(appContext, vJTitulo, vJCategoria, vJPrecio, vJDesarrollador, vJDescripcion);
                exito = (id != -1L);
            }


            return  (exito);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }
    }

}
